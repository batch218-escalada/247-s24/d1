console.log('Hello World');

// ES6 - ECMAScript

// [SECTION] Exponent Operator

    const firstNum = 8 ** 2;
    console.log(firstNum);

// [SECTION] Template Literals

    // Allows to write string without using the concateenation operator (+).

    let name = 'John';

    // [SUB-SECTION] Pre-Template Literal String
    // Uses single quotes ('')

    let message = 'Hello ' + name + ' Welcome to programming!';
    console.log('Message without template literals: ' + message);

    // [SUB-SECTION] Strings Using Template Literals
    // Uses backticks (``)

    message = `Hellow ${name}! Welcome to programming!`;
    console.log(`Message with template literals: ${message}`);

    // [SUB-SECTION] Multi-line Using Template Literals
    const anotherMessage = `
    ${name} attended a math compatition.
    He won it by solving the problem 8 ** 2 with
        the solution of ${firstNum}.
    Observe the 'enter'.`

    console.log(anotherMessage);

    // Template literals allows us to write strings with embedded JS expression.

    const interestRate = .1;
    const principal = 1000;

    console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring

    // Allows to unpack elements in arrays into distinct variables.

    const fullName = ['Juan', 'Dela', 'Cruz'];

    // Pre-Array Destructuring
    console.log(fullName[0]);
    console.log(fullName[1]);
    console.log(fullName[2]);

    console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

    // Array Destructuring
    const [firstName, middleName, lastName] = fullName;

    console.log(firstName);
    console.log(middleName);
    console.log(lastName);

    console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// [SECTION] Object Destructuring

    // Allows to unpack properties of objects into distinct variables.

    const person = {
        givenName: 'Jane',
        maidenName: 'Dela',
        familyName: 'Cruz',
    };

    // Pre-Object Destructuring
    
    console.log(person.givenName);
    console.log(person.maidenName);
    console.log(person.familyName);

    console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

    // Object Destructuring
    const { givenName, familyName, maidenName } = person;

    console.log(givenName);
    console.log(familyName);
    console.log(maidenName);

    console.log(`Hellow ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

    function getFullname({givenName, familyName, maidenName}) {
        console.log(`${givenName} ${maidenName} ${familyName}`);
    };

    getFullname(person);

// [SECTION] Arrow Function

    // Compact alternative suntax to traditional functions

    const hello = () => {
        console.log(`Hellow Worlds!`);
    };

    hello();

    // Pre-Arrow Function and Template Literals

    // function printFullName (firstName, middleInitial, lastName) {
    //     console.log(firstName + ' ' + middleInitial + '. ' + lastName);
    // };

    // printFullName('John', 'D', 'Smith');

    const printFullName = (firstName, middleInitial, lastName) => {
        console.log(`${firstName} ${middleInitial}. ${lastName}`)
    };

    printFullName('John', 'D', 'Smith');

    // [SUB-SECTION] Arrow Functions with Loops

    const students = ['John', 'Jane', 'Judy'];

    students.forEach ( function (student) {
        console.log(`${student} is a student.`);
    });

    // Arrow Function
    // The function is only used in te 'forEach' method to print out a text with the student's names.

    students.forEach((student) => {
        console.log(`${student} is a student.`);
    });

// [SECTION] Implicit Return Statement

    // There are instances when you can omit the 'return' statement.

    // Pre-Arrow Function

    // const add = (x, y) => {
    //     return x + y;
    // };
    // let total = add(1, 2);
    // console.log(total);

    // Arrow Function

        const add = (x, y) => x + y;

        let total = add(1, 2);
        console.log(total);

// [SECTION] Default Function Argument Values

    // Provides a default argument value if none is provided when the function is invoked.

        const greet = (name = 'User') => {
            return `Good morning, ${name}!`;
        };

        console.log(greet());
        console.log(greet('John'));

// [SECTION] Class-Based Object Blueprints

        // Allows creating/instantiation of objects using classes as blueprints.

        // Creating a Class
            // The constructor is a special method of a class for creating/initializing an object for the class.

            /* 
            Syntax:
                class className {
                    constructor(objectPropertyA, objectPropertyB) {
                        this.objectPropertyA = objectPropertyA;
                        this.objectPropertyB = objectPropertyB;
                    };
                };
            */
                class Car {
                    constructor(brand, name, year) {
                        this.brand = brand;
                        this.name = name;
                        this.year = year;
                    }
                };

            // Values of properties may be assigned after creation/instantiation of an object.
            const myCar = new Car()
            console.log(myCar);

                myCar.brand = 'Ford';
                myCar.name = 'Ranger Raptor';
                myCar.year = 2022;

                console.log(myCar);
            
            // Creating/instantiating a new object from the car class with initialized values.

            const myNewCar = new Car ('Toyota', 'Hilux', 2022);
            console.log(myNewCar);